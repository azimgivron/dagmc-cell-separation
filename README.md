# README for DAGMC Cell Separation Example

This example demonstrates a scenario in DAGMC (Direct Accelerated Geometry Monte Carlo) where separating a single cell into multiple cells necessitates the generation of new `.h5m` files with renamed materials. This process can be particularly cumbersome due to the significant disk space required for storing multiple `.h5m` files, each differing only in material names.

## Overview

The script provided outlines a process to simulate the separation of a single geometric cell into multiple cells within a DAGMC model. Initially, a model with a singular cell is created. Subsequently, this model is altered to contain multiple cells by generating new `.h5m` files where each file represents a fraction of the original geometry with uniquely named materials.


## Dependencies

- `openmc 0.14.0` - An open-source Monte Carlo particle transport code.
- `dagmc` - A toolkit for using CAD-based geometries in Monte Carlo simulations.

## Key Components

### RectangularBasisPrism

A class representing a finite rectangular prism defined by its dimensions and boundary conditions. This prism is used as the basic geometry for cell creation.

### generate_cells

A function that generates DAGMC cells from a given `.h5m` file and a geometric shape.

### generate_first_model

Generates an `openmc.Model` with a single cell in the geometry. This initial model represents the state before any cell division.

### generate_snd_model

Generates an `openmc.Model` with multiple cells by dividing the initial geometry. This process involves creating new `.h5m` files for each sub-cell with uniquely named materials, showcasing the cumbersome aspect of the cell division process in DAGMC.

### generate_plots

Creates visualizations of the geometric model at different stages of the process.

#### Results
##### Before cells seperation
![alt text](yz_before.png "Before cells seperation")
##### After cells seperation
![alt text](yz_after.png "After cells seperation")

## Usage

1. **Prepare the Environment**: Ensure that `openmc` and `dagmc` are installed and accessible in your Python environment.
2. **Run the Script**: Execute the script in an environment where `openmc` simulations can be run. The script will:
   - Extract the initial geometry from a `.tar.gz` archive if necessary.
   - Generate the initial model with a single cell and create visualizations.
   - Generate a second model with the geometry divided into multiple cells, each with uniquely named materials, and create visualizations for this state as well.

## Limitations

This example illustrates the disk space inefficiency and operational inconvenience of generating new `.h5m` files for each minor modification in the model, such as renaming materials to differentiate between divided cells. It is intended to highlight a specific challenge within the DAGMC workflow and is not representative of all potential use cases or optimizations that may mitigate the described limitations.