import math
import os
import os.path
import subprocess
from collections import namedtuple
from typing import Any, List, Tuple

import dagmc
import openmc

PrismBoundaryType = namedtuple(
    "PrismBoundaryType", ["x_pos", "x_neg", "y_pos", "y_neg", "z_pos", "z_neg"]
)
Offsets = namedtuple("Offsets", ["x0", "y0", "z0"])


class RectangularBasisPrism:
    """Finite rectangular basis prism.

    Attributes:
        x_pos (openmc.XPlane): Right delimitation.
        x_neg (openmc.XPlane): Left delimitation.
        y_pos (openmc.YPlane): Front delimitation.
        y_neg (openmc.YPlane): Back delimitation.
        z_pos (openmc.ZPlane): Top delimitation.
        z_neg (openmc.ZPlane): Bottom delimitation.
        delta_x (float): Length in the X direction.
        delta_y (float): Length in the Y direction.
        delta_z (float): Length of the prism (Z direction).

    """

    def __init__(
        self,
        delta_x: float,
        delta_y: float,
        delta_z: float,
        boundary_type: PrismBoundaryType = PrismBoundaryType(
            "vacuum", "vacuum", "vacuum", "vacuum", "vacuum", "vacuum"
        ),
        offsets: Offsets = Offsets(0, 0, 0),
    ):
        """Constructor.

        Args:
            delta_x (float): Length in the X direction.
            delta_y (float): Length in the Y direction.
            delta_z (float): Length of the prism (Z direction).
            boundary_type (PrismBoundaryType, optional):  Boundary conditions, respectively x_pos, x_neg, y_pos, y_neg, z_pos, z_neg.
                                                    (default: PrismBoundaryType("vacuum", "vacuum", "vacuum", "vacuum","vacuum","vacuum"))
            offsets (Offsets, optional): Offsets in the different directions, respectively X, Y and Z.
                                        (default: Offsets(0,0,0))

        """
        self.delta_y = delta_y
        self.delta_x = delta_x
        self.delta_z = delta_z
        self.cartesian_coordinates = offsets
        self.x_pos = openmc.Plane(
            1, 0, 0, delta_x / 2 + offsets.x0, boundary_type=boundary_type.x_pos
        )
        self.x_neg = openmc.Plane(
            1, 0, 0, -delta_x / 2 + offsets.x0, boundary_type=boundary_type.x_neg
        )
        self.y_pos = openmc.Plane(
            0, 1, 0, delta_y / 2 + offsets.y0, boundary_type=boundary_type.y_pos
        )
        self.y_neg = openmc.Plane(
            0, 1, 0, -delta_y / 2 + offsets.y0, boundary_type=boundary_type.y_neg
        )
        self.z_pos = openmc.Plane(
            0, 0, 1, delta_z / 2 + offsets.z0, boundary_type=boundary_type.z_pos
        )
        self.z_neg = openmc.Plane(
            0, 0, 1, -delta_z / 2 + offsets.z0, boundary_type=boundary_type.z_neg
        )

    def __neg__(self) -> openmc.Intersection:
        """Unary minus.

        Returns:
            openmc.Intersection: Inside the prism.

        """
        return (
            +self.x_neg
            & -self.x_pos
            & +self.z_neg
            & -self.z_pos
            & -self.y_pos
            & +self.y_neg
        )

    def __pos__(self) -> openmc.Intersection:
        """Unary plus.

        Returns:
            openmc.Intersection: Outside the prism.

        """
        return ~(-self)


def generate_cells(dag_file: str, shape: Any) -> List[openmc.Cell]:
    """Generate the cells.

    Returns:
        List[openmc.Cell]: The cells.

    """
    dagmc_univ = openmc.DAGMCUniverse(dag_file, auto_geom_ids=True)
    cell = openmc.Cell(region=-shape, fill=dagmc_univ)
    return [cell]


def generate_first_model(h5m_path: str, dim: float) -> openmc.Model:
    """Generate model with a single cell in the geometry.

    Args:
        h5m_path (str): The path to the h5m path.
        dim (float): Length of a side of the boundary box.

    Returns:
        openmc.Model: The model.

    """
    cells = generate_cells(h5m_path, RectangularBasisPrism(dim, dim, dim))
    mat1 = openmc.Material(name="fuel")
    mat1.add_element("U", 1)
    mat2 = openmc.Material(name="atmosphere")
    mat2.add_element("Na", 1)
    mat3 = openmc.Material(name="structure")
    mat3.add_element("He", 1)

    return openmc.Model(
        materials=openmc.Materials([mat1, mat2, mat3]),
        geometry=openmc.Geometry(
            root=openmc.Universe(cells=cells), merge_surfaces=True
        ),
        settings=openmc.Settings(),
    )


def generate_offsets_and_boundaries(
    dim: float,
) -> Tuple[List[Offsets], List[PrismBoundaryType]]:
    """Generate the offsets and the boundaries of the sub-cells.

    Args:
        dim (float): Length of a side of the boundary box.

    Returns:
        Tuple[List[Offsets], List[PrismBoundaryType]]: Offsets and boundaries.

    """
    centers = [
        Offsets(dim / 4, dim / 4, dim / 4),
        Offsets(-dim / 4, dim / 4, dim / 4),
        Offsets(dim / 4, -dim / 4, dim / 4),
        Offsets(dim / 4, dim / 4, -dim / 4),
        Offsets(-dim / 4, -dim / 4, dim / 4),
        Offsets(-dim / 4, dim / 4, -dim / 4),
        Offsets(dim / 4, -dim / 4, -dim / 4),
        Offsets(-dim / 4, -dim / 4, -dim / 4),
    ]
    boundary_map = {1: ["vacuum", "transmission"], -1: ["transmission", "vacuum"]}
    boundaries = []
    for offset in centers:
        values = []
        for val in offset:
            values.extend(boundary_map[math.copysign(1, val)])
        boundaries.append(PrismBoundaryType(*values))
    return centers, boundaries


def generate_snd_model(h5m_path: str, dim: float) -> openmc.Model:
    """Generate model with 8 cells in the geometry.

    Args:
        h5m_path (str): The path to the h5m path.
        dim (float): Length of a side of the boundary box.

    Returns:
        openmc.Model: The model.

    """
    cells = []
    offsets_list, boundaries = generate_offsets_and_boundaries(dim)
    materials = []
    for i, (offsets, boundary_type) in enumerate(zip(offsets_list, boundaries)):
        file = f"{h5m_path[:-4]}-{i}.h5m"
        if not os.path.exists(file):
            model = dagmc.DAGModel(h5m_path)
            model.groups["mat:fuel"].name = f"mat:fuel-{i}"
            model.groups["mat:structure_comp"].name = f"mat:structure-{i}_comp"
            model.groups["mat:atmosphere"].name = f"mat:atmosphere-{i}"
            model.mb.write_file(file)
        mat1 = openmc.Material(name=f"fuel-{i}")
        mat1.add_element("U", 1)
        mat2 = openmc.Material(name=f"atmosphere-{i}")
        mat2.add_element("Na", 1)
        mat3 = openmc.Material(name=f"structure-{i}")
        mat3.add_element("He", 1)
        materials.extend([mat1, mat2, mat3])
        box = RectangularBasisPrism(
            dim / 2, dim / 2, dim / 2, offsets=offsets, boundary_type=boundary_type
        )
        cells.extend(generate_cells(file, box))

    return openmc.Model(
        materials=openmc.Materials(materials),
        geometry=openmc.Geometry(
            root=openmc.Universe(cells=cells), merge_surfaces=True
        ),
        settings=openmc.Settings(),
    )


def generate_plots(output_path: str) -> openmc.Plots:
    """Generate plots.

    Args:
        output_path (str): The output path.

    Returns:
        openmc.Plots: The plots.

    """
    p = openmc.Plot()
    p.width = 50, 50
    p.basis = "yz"
    p.origin = 0.0, 0.0, 0.0
    p.pixels = 500, 500
    p.color_by = "material"
    p.filename = f"{output_path}/yz.png"
    p.name = "yz plane at x=0.\n(material coloring)"

    p2 = openmc.Plot()
    p2.width = 50, 50
    p2.basis = "xy"
    p2.origin = 0.0, 0.0, 0.0
    p2.pixels = 500, 500
    p2.color_by = "material"
    p2.filename = f"{output_path}/xy.png"
    p2.name = "xy plane at x=0.\n(material coloring)"

    p3 = openmc.Plot()
    p3.width = 50, 50
    p3.basis = "xz"
    p3.origin = 0.0, 0.0, 0.0
    p3.pixels = 500, 500
    p3.color_by = "material"
    p3.filename = f"{output_path}/xz.png"
    p3.name = "xz plane at x=0.\n(material coloring)"

    return openmc.Plots([p, p2, p3])


if __name__ == "__main__":
    if not os.path.exists(os.path.abspath("Radis")):
        command = ["tar -xvzf radis.tar.gz"]
        subprocess.run(command, shell=True, check=True, capture_output=True)

    output_path = os.path.abspath("out")
    first_output_path = f"{output_path}/before_cut"
    snd_output_path = f"{output_path}/after_cut"
    h5m_path = os.path.abspath("Radis/h5m/radis.h5m")

    os.makedirs(output_path, exist_ok=True)
    os.makedirs(first_output_path, exist_ok=True)
    os.makedirs(snd_output_path, exist_ok=True)

    dim = 25
    model = generate_first_model(h5m_path, dim)
    model.plots = generate_plots(first_output_path)
    model.export_to_xml(directory=first_output_path)
    model.plot_geometry(cwd=first_output_path)

    # then we mesh the geometry to get multiple cells
    # the box is cut in 4 sub boxes
    dim = 25
    model = generate_snd_model(h5m_path, dim)
    model.plots = generate_plots(snd_output_path)
    model.export_to_xml(directory=snd_output_path)
    model.plot_geometry(cwd=snd_output_path)
